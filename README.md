# README #

Calibrate a continous servo with a fixed voltage divider.
Adjust the vel value until the motor comes to a stop. This done over serial.
Then record and use this new value in subsequent programs.

https://hackaday.io/project/17079-mocoder-magnetic-encoder

Created by Nick Stedman 2016, nick@robotstack.com.