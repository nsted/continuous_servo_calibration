/*
  Calibrate a continous servo with a fixed voltage divider.
  Adjust the vel value until the motor comes to a stop. This done over serial.
  Then record and use this new value in subsequent programs.

  https://hackaday.io/project/17079-mocoder-magnetic-encoder
  
  Created by Nick Stedman 2016, nick@robotstack.com.  
*/

#include <Servo.h>

Servo myservo;  // create servo object to control a servo

int val;    // variable to read the value from the analog pin

void setup() {
  Serial.begin(9600);
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object
}



// Adjust the vel value until the motor comes to a stop. This done over serial.
// Then record and use this new value in subsequent programs.

long vel = 1500;
long lastVel = 0;

void loop() {
  // retrieve user specified destination if available
  vel = getIntFromSerial(vel);
  if (vel != lastVel) {
    myservo.writeMicroseconds(vel);    // sets the servo speed
    Serial.println(vel);
  }
  delay(15);                           // waits for the servo to get there
  lastVel = vel;
}




// Retrieve a signed integer from the Serial port.
// Value should be sent with CR & NL

String sInString = "";

long getIntFromSerial(long lastVal) {
  long newVal = lastVal;

  while (Serial.available() > 0) {
    int inChar = Serial.read();

    if (isDigit(inChar) || inChar == '-') {
      sInString += (char)inChar;
    }
    else if (inChar == '\n') {
      newVal = sInString.toInt();
      sInString = "";
    }
    else if (inChar == '\r') {
    }
    else {
      sInString = "";
    }
  }
  return newVal;
}
